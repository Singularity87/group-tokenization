# CHIP-2021-02 Group Tokenization for Bitcoin Cash

        Title: Group Tokenization for Bitcoin Cash
        First Submission Date: 2021-02-23
        Authors: Group Tokenization, Andrew Stone.
                 This proposal, A60AB5450353F40E.
        Type: Technical
        Layer: Consensus
        Status: DRAFT
        Last Edit Date: 2021-03-14

## Summary

The proposal updates the Bitcoin Cash consensus to natively support alternative currencies without impeding on the utility of native currency (BCH) by upgrading the peer-to-peer cash system of Bitcoin in such way that every other currency transaction output creation marginally increases demand for the native curreny (BCH) thus remaining faithful to design precepts from the Bitcoin whitepaper.

## Motivation

The first real use of Ethereum smart contracts was to implement tokens.
From there the market has seen a token cambrian explosion, which ironicaly made smart contracts useful.
Most tokens are used to represent some form of cash.
The market demands smart cash but, with Ethereum, it got smart contracts disguised as cash.
We see this as an opportunity where Bitcoin Cash could gain competitive advantages while staying true to the vision of the best digital cash, thus improving utility and increasing the value of Bitcoin Cash: The Peer-to-Peer Electronic Cash System for Planet Earth.

Smart contracts provide the verbs of a language, and tokens are the nouns.
A language cannot express much with just one noun but the reverse isn't true.
Tokens with only 4 verbs (create, transfer, mint, burn) are useful alone, because they can express fiat currency, stocks, bonds, tickets, real estate deeds, and much more.
The popularity of stablecoins has shown the demand for tokenized fiat currency and those 4 verbs alone enabled blockchains to become more tightly integrated with other financial systems.
Smart contracts on top are mostly used only to add security in interaction between currencies when interaction is to be done in a decentralized and trustless way.

Bitcoin Cash already has some verbs implemented through Script, but it has just one noun.
We believe that it needs more nouns in order to expand utility and grow in value, that it needs smart cash with select specialized verbs before it will need more general verbs.
We believe those other nouns need to have the same simplicity and ease of use as the first noun, and that the fundamental verbs must be provided by the blockchain itself.
After all, it's hard to make a sentence if you're forced to use a combination of verbs as a substitute for nouns.

## Implementation Costs and Risks

The solution is conceptually simple.
The entire implementation showcasing it can fit into a single `.cpp` file consisting of a single function using simple language structures and which can fit well under ~400 lines of C++.
This number comes from an actual implementation that was focused on understandability, not succinctness.
By reusing existing loops and other techniques, it can be implemented in even fewer lines. This kind of solution is easy to implement and reason about.

Downstream, the users can rely on the same design patterns as Bitcoin uses which have been successfully tested since 2009, and that will be demonstrated in the **[Technical Description](#technical-description)** section of this document.

Every change to the source code implies the possibility of errors.
This is true for every project.
The alternative is obsolescence.
However, Group Tokenization relies on no complex cryptography, and no complex algorithms.
At its heart, it simply verifies that tokens balance in a transaction, just like the software already does for BCH!
We cannot invent problems with this approach just to satisfy the reader looking for problems.
If we have missed something in our reasoning, we will be happy to hear from you.

## Ongoing Costs and Risks

As above.

## Technical Description

This section intends to help the reader build a mental model of group tokenization.
It is intended for illustrative purposes and the careful, authoritative specification is located in the [Specification](#specification) section.

Bitcoin Cash transactions are designed to move money, always in the form of its native token labeled BCH and using the atomic unit "satoshi".
This proposal intends to enable movement of other tokens using the same means as BCH.

We introduce an optional dual-token output system, which will carry both BCH and another token.
Previous transaction outputs are used as inputs to a new transaction where they will be unlocked and consumed, and their BCH and token balance moved to newly created unspent dual-token transaction outputs.

These new types of outputs can be freely mixed and combined with any number of dual (token & BCH) and single (BCH) inputs and outputs in the same transaction.
The main rule is that the amounts in, summed for each disctinct token, have to be exactly the same as amounts sent out.
The existing validation rules for BCH are unchanged, which means that BCH can freely flow between single and dual outputs, and that only for BCH are the fees subtracted.
Because of the existing dust limit rule, dual outputs will need to always carry a minimum amount of BCH.

We introduce two additional optional fields into BCH outputs, a `Group ID` to distiguish other tokens, and a `Group quantity` to record their amounts.
These are stored as two pushes in the output script followed by OP_GROUP opcode which pops them.
The stack is left unchanged, and the data is written to the output.
This is proposed as a way to avoid changing the transaction format.

![fig1](./CHIP-2021-02_fig1.png)

Group tokens are almost not special in comparison to BCH, which means that all the power of SPV, SIGHASH, Script and future innovations are directly available to them.

With the above rules we have created a token transfer system, but we don't have a way for tokens to enter or escape that system i.e. token units need to be minted or destroyed somehow.
To achieve that we need a way to locally, within the scope of a single TX, break the global rules.

For this we introduce the concept of token authority outputs.
On the outside they're the same as any other grouped outputs but the quantity field is repurposed to encode authority bits instead of token amount.
Presence of an authority bit in the inputs will allow local rule-breaking.
So far we have introduced only that one token balancing global rule, so we need MINT and MELT authority bits as a way to locally break it.
We call it melting to disambiguate from burning the entire dual output which would also burn the BCH.
Melt entirely frees the BCH and only burns the token part.

This will be a fundamental principle to enable other operations: when we introduce a globally constraining rule, we also provide an authority output type to locally break it.

Here we must introduce another global rule: An authority output cannot carry itself to the outputs side.
This makes our MINT and MELT authorities one-time use i.e. they get destroyed with use, so how do we carry them to the other side?
We again locally break the global rule by introducing the BATON authority, the authority to create authorities.
Presence of a BATON alongside another authority allows both authorities to be carried over.
With this, we have created a token authority transfer system.

What's left is to regulate the way authorities can enter the system (they can freely exit).
Here we introduce another global rule: an authority cannot be created from nothing.
We will allow this rule to be locally broken only in the genesis transaction -- one time for each distinct group ID.
To enforce this we require the group ID to be generated by hashing the genesis transaction data, which will ensure uniqueness by making it computationally impossible to create a duplicate token genesis.

With this we have created a token system that allows us to: create a token, manage the token supply, and transfer both the tokens and token management authorities.
Recall that grouped outputs, both ordinary and authority, can all use the power of SPV, SIGHASH, and Script.
With those we can achieve great versatility in how tokens and their authorities are passed around, as demonstrated in the [examples](#example-uses) section.

### Advanced Features

Here we extend group token features in a similar manner as previously: constraining globally and relaxing locally.

#### Subgroups

We allow extending the group ID up to a total 64 bytes, where the first 32 bytes are permanently set in the parent group genesis, and the extended bytes are arbitrary.
Any token with a group ID larger than 32 bytes is a subgroup token.
A subgroup token inherits all the properties of the parent group but it is a distinct token.
All of the previously established rules for group tokens apply to these extended tokens.

Therefore, subgroup token units can't be simply created from parent token units or their authorities beause it would violate previously established rules i.e. balancing by group ID.
The parent is another token, so it is not fungible with subgroup tokens.

To break these balancing rules locally, we introduce the SUBGROUP authority.
Paired with a MINT or MELT group authority it can create or destroy units of any subgroup.
For example, a new subgroup token is created by having MINT | SUBGROUP authority of the parent group in the inputs, and it will allow creation of token units which will have the extended group ID.

Adding BATON to that parent group authority will allow it to create subgroup authorities which can then only operate on that particular subgroup.
Subgroup authorities are created the same way as subgroup tokens, simply by having the required authority in the inputs and appending arbitrary data to the group ID in the outputs.

This allows for creating multiple tokens of a same class e.g. a NFT token series that could all be identified as belonging to the same series simply by inspecting the parent group ID and the appended data could reference their serial No. or a document hash.
All MINT authorities could then be destroyed, and the NFT units would then become provably unique.

#### Optional Global Group Constraints

So far, all the global constraints we introduced are fixed for all groups.
Here we introduce a way for the user to set some additional constraints that will enable more utility.
Optional group constraints are recorded in 2 bytes of the Group ID and can't be changed after creation of the group.
Colliding group ID, which is computationally impossible, with all but those 2 bytes would still be an entirely different token, because the settings are part of group definition -- different settings, different group.
To conserve group ID entropy, setting those 2 bytes requires rolling a nonce at group genesis until we get the desired match, similarly to how Bitcoin Cash vanity addresses are made.

#### Hold BCH

This feature is introduced through toggleable global group constraining, and is set at group genesis.
It modifies the group behavior to create a "fence" around some amounts of BCH, and disables the group token quantity field by constraining it to 0.
Essentially this enables creation of colored BCH.
With this, any BCH found in group outputs is trapped there and can only move between outputs of the same group.
The way to locally break this global rule is through existing MINT and MELT authorities, which can bring the BCH in & out of the fence.

#### Covenant

This feature is introduced through toggleable global group constraining, and is set at group genesis.
It modifies the group behavior to require the hash of new outputs locking script match that of the first input of the same group.
To locally break the rule, we introduce a RESCRIPT authority, which will allow the script hash of the outputs to be freely set in that transaction.
This means that we could have outputs of the same group with different script encumberances, and as a consequence the recipients will have a choice of which encumberance to accept -- by placing a token unit with the desired contract in the first input.

In practice, this means that the authority holder cannot force a more restrictive script on existing token holders and that he could only offer them a less restrictive one.
The holders could accept the new contract by spending an unit of that token as a first input, and including all old contract tokens as folllowing inputs.
Then, the full amount of their holdings would be converted to a new contract.

For this to be useful, locking script must factor out all the variables so that the hash will remain the same for all outputs with the same template.
The variables are to be provided with the unlocking script at the time of spending.

TBD: clarify what's required to enable the utility, can it be done within current script templates or do we need to include a few more? Main doc. says "This would not be a consensus rule change, except for the P2SH “magic” where the redeem script is executed.", what is the meaning of this "magic"?

### Design Process Timeline

2017-10-16 Introduction

2017-11-19 BUIP

...

We could add snapshots of the past ideas and the path it took to get us here.
After all, the idea converged towards this design through multiple cycles.
Would be good to have it all referenced somewhere.

TBD

## Specification

The specification below is copied from the orignal repo hosted here:

[Group Tokenization Consensus Specification](https://github.com/bitcoin-unlimited/BUwiki/blob/master/grouptokenization/groupbchspec.md)

and matches commit [880d693](https://github.com/bitcoin-unlimited/BUwiki/commit/880d69330623e4eb4e0b068c7361c6c60494e9e7)
word for word, with only some aesthethic tweaks to fit it into the CHIP body.

This section describes the blockchain and consensus changes required to implement Group Tokenization on Bitcoin Cash.  It is intended for implementers, and generally does not explain why certain decisions were made.  It assumes that you understand the general architecture and purpose of Group tokens. If, while you are reading this section, you need greater understanding of a topic, please refer to the main [Group Tokenization proposal document](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit?usp=sharing) or the 
[Technical Description](#technical-description) section of this CHIP.

**Notes on reading this section**:

 * Industry-standard conventions with respect authoritative and normative statements are followed; that is, use of capital MUST, MUST NOT, et. al.
 * Testable requirements are specified with an identifier (REQX.Y.Z) for use when building unit tests.

### 1 Blockchain Changes

Group Tokenization fits its changes within the existing transaction format by prefixing transaction output scripts with Group data. This makes the changes required to implement group tokens very small and self contained<sup>1</sup>.

#### 1.2 Transaction Changes

Within a transaction, grouped outputs follow the existing output serialization format, except that the output script is formatted as follows. Following normal convention <\> indicates a data push. Also [] indicates optional arbitrary additional script code:

<GroupID\> <QuantityOrFlags\> OP_GROUP [any normal constraint script]

### 2 Script Machine Changes

OP_GROUP may appear anywhere within a script.

OP_GROUP is a new opcode defined as the byte 0xee (REQ1.1.0).

Execution of the OP_GROUP opcode MUST pop 2 objects from the main stack (REQ1.1.1).

If there are fewer than 2 objects on the stack, execution MUST fail (REQ1.1.2).

Optimizing script machines may recognize the push,push,OP_GROUP pattern and skip execution.  However, such a machine MUST behave exactly as if those instructions were executed including failing the script due to bounds checking (both during the pushes and the OP_GROUP) and properly accounting for these op codes in any tallies (REQ1.1.3).

The following is an example "C" implementation of the OP_GROUP instruction:

                case OP_GROUP: // OP_GROUP just pops its args during script evaluation
                    if (stack.size() < 2)
                        return set_error(serror, SCRIPT_ERR_INVALID_STACK_OPERATION);
                    popstack(stack);
                    popstack(stack);
                    break;

### 3 Transaction Validation Changes

All existing transaction validation rules are unchanged.

A UTXO is defined as "grouped" if it contains a serialized script following the format specified in section 1.2. An OP_GROUP instruction that appears in any other place in the serialized script does not make a UTXO "grouped".

In other words, for Group semantics to apply to a UTXO, the "real" script must be **prefixed** by OP_GROUP instruction and its arguments.  OP_GROUP is therefore semantically an attribute of the UTXO that is actually used *outside* of the script interpreter.  Inside the interpreter, its has no meaning except to pop 2 elements off of the stack.  It is structured in this manner to minimize changes to the transaction format, and therefore changes required by the BCH ecosystem.  A non-tokenized, un-upgraded light wallet will understand the transaction and will even be able to spend non-grouped UTXOs from a transaction that has grouped UTXOs.  But it will not understand the Grouped UTXOs.  This is a feature that prevents unupgraded light wallets from accidentally spending potentially valuable tokens for their dust BCH.

If the "QuantityOrFlags" most significant bit is clear (the number is positive), this field should be interpreted as a quantity.  If the MSB is set, this is an "authority baton UTXO" and this field should be interpreted as Flags (described below).

#### Flags

##### Authority Permission Flags

AUTHORITY = 1<<63, *This is an authority UTXO, not a "normal" quantity holding UTXO*  
MINT = 1ULL << 62, *Can mint tokens*  
MELT = 1ULL << 61, *Can melt (destroy) tokens*  
BATON = 1ULL << 60, *Can create authority outputs*  
RESCRIPT = 1ULL << 59, *Can change the output script*  
SUBGROUP = 1ULL << 58, *Can operate on subgroups*

ACTIVE_FLAG_BITS = AUTHORITY | MINT | MELT | BATON | RESCRIPT | SUBGROUP  
ALL_FLAG_BITS = (0xffffULL << (64 - 16))  
RESERVED_FLAG_BITS = ACTIVE_FLAG_BITS & ~ALL_FLAG_BITS

Reserved flag bits 57 to 48 are reserved for future hard fork group features and must be zero.  Bits 0 through 47 are used as a nonce in the Group genesis UTXO, but must be zero in other authority UTXOs.

##### Group flags

Group Identifiers are 32 byte values<sup>2</sup>.  The last 2 bytes of a group identifier<sup>4</sup> indicate properties of the group as follows:

COVENANT = bit 0,  *This group enforces the structure of output scripts*  
HOLDS_BCH = bit 1,  *This Group holds BCH instead of tokens*  
GROUP_RESERVED_BITS = bits 2 through 15, MUST be 0.

For example, if the "groupId"  is a byte array, the COVENANT bit is groupId[31]&1.

#### 3.1 Subgroups

Subgroups are groups with a parent group.  Parent authorities can execute operations on subgroups.  Subgroup Identifiers are the byte concatenation of the 32 byte parent group identifier and arbitrary additional bytes, limited by the maximum stack element size.  Subgroups do not have their own flags; they have the same flags at the same location as their parent group.

Therefore to determine the parent group identifier from a subgroup identifier, simply take the first 32 bytes.

#### 3.2 Transaction Group Enforcement

Certain properties are enforced across the inputs and outputs of a transaction.  These properties only apply to "grouped" inputs and outputs.  If a transaction has no grouped inputs or outputs an optimized validator may return VALID without further analysis.

This section enforces the following general rules:

 * for the same group, input quantities must match output quantities (unless overridden by authorities),
 * authority baton permissions are properly passed from inputs to outputs and from group to subgroup,
 * output scripts must match previous output scripts for covenanted groups, and
 * newly created groups are created correctly.

This is not a lot of rules and the resulting algorithms are not complex.  However, the following sections specify the exact rules in great detail, covering every tiny edge condition since any differences might cause a consensus failure.  So upon the first read, these sections may make the rules seem daunting.  But actually, this document is much larger than a quality implementation which can fit into a single function and a few helpers.  Just keep your mind on the overall picture stated in the previous paragraph and work through the rules slowly.  A good approach is to loop through outputs then inputs collecting data on each group.  Then loop through the collected groups, verifying the rules based on data collected.  If you come upon an edge condition that seems unspecified make an in-code note that will result in a compilation error.  DO NOT GUESS OR SKIP!  Contact an author of this paper and we will clarify the requirement.

**Notes on reading these rules**:

* All operations proceed on a per-group basis and so "per GroupId" is implied in every statement.
* References to "group inputs" and "outputs" never include the authorities -- authorities are special and are always specifically referenced.
* Grouped UTXOs have 2 fields: a "GroupId" and "Quantity", and Grouped authority baton UTXOs have 2 fields: "GroupId" and "Flags", as described in section 3.1.
* The term "GroupId" refers to both groups and subgroups (a subgroup IS a group).
* Non-compliance with any of these rules results in an immediate return of INVALID.

For example, the phrase "input xxx authority" (e.g "input mint authority") means "an input group authority with the xxx Flag bit set", or to be extremely pedantic, "a transaction input whose prevout UTXO scriptpubkey begins with <GroupID\> <QuantityOrFlags\> OP_GROUP, and whose GroupID field matches the one we are currently working on and whose QuantityOrFlags field has the AUTHORITY (63) and MINT (62) bits set".

Likewise, the phrase "sum of the output Quantities" should be pedantically understood as "the sum of the Quantity fields in the non-authority-baton outputs whose GroupID field matches the one we are currently working on".

##### Properly Constructed Group Annotations

* For every output that begins with <push A\> <push B\> OP_GROUP:
	* Length A MUST be >= 32 bytes
	* Length B MUST be 2, 4, or 8 bytes
* The QuantityOrFlags field is constructed from B as follows:
	* If Length B is 2 or 4 bytes, deserialize as a little-endian 2 or 4 byte unsigned number and zero extend to a 64 bit integer.
	* If Length is 8 bytes, deserialize as a little-endian 64 bit signed integer
* If The QuantityOrFlags field is negative (has bit 63 set) it is an Authority Baton.  *(There's no such thing as a negative quantity, even though the Quantity is signed)*

Note: These deserialization rules mean that the only way to specify an Authority Baton UTXO is when B has a length of 8 bytes.

Note: If OP_GROUP appears as a proper script prefix, both these rules and the script machine rules apply. If OP_GROUP is used in any other place within the script, only the script machine rules are applied.  This means that its valid to have an OP_GROUP in some other location within a script whose arguments have any length.  Even though this makes specification more complex, it makes validation simpler.

##### Setup

Construct 4 64 bit bitmaps (per group that appears in the transaction's inputs and outputs): perms, batonPerms, subgroupPerms and subgroupBatonPerms.  Initialize them to 0.

##### Authority Permission Accumulation

* for every input authority perms = perms | Flags *(Figure out the permissions available for group operations)*
* For every input authority with the Baton bit set, batonPerms = batonPerms | Flags *(figure out what authorities can be passed to child authorities)*
* For every input authority with the Subgroup bits set, subgroupPerms = subgroupPerms | Flags *(Figure out all permissions available on the subgroup due to group authorities)*
* For every input authority with the Baton and Subgroup bits set, subgroupBatonPerms = subgroupBatonPerms | Flags *(Figure out all permissions that can be granted to subgroup authorities due to group authorities)*

Note that the SUBGROUP authority bit means that the other permission bits in this authority apply to both this group and any of its subgroups, not exclusively to subgroups.  Authorities can be targeted exclusively to a subgroup via the "baton" system by creating a child authority with the subgroup's ID.

##### Subgroup Permission Accumulation

* For every subgroup, look up its corresponding group.  Set subgroup.batonPerms = subgroup.batonPerms | (the group's subgroupBatonPerms& ~SUBGROUP). *(Extend SUBGROUP-tagged authority permissions to the subgroup.  But do not extend subgrouping permissions itself, because there's no such thing as recursive subgroups)*
* For every subgroup, look up its corresponding group.  Set the subgroup.perms = subgroup.perms | group.subgroupPerms *(Extend SUBGROUP-tagged permissions to subgroups)*

##### 3.1.2.1 Group Genesis Rule

* If there is > 1 group (excluding subgroups) that has output authorities but batonPerms==0, return INVALID *(We only allow one group Genesis per transaction)* (REQ3.2.1.1)
* For that output authority:
	* If the transaction has other grouped (authority or normal) outputs for this group, return INVALID *(the genesis authority is the only use of this group allowed in this transaction)* (REQ3.2.1.2)
	* Ensure that the RESERVED_FLAG_BITS are 0 (REQ3.2.1.3)
	* Ensure that the GROUP_RESERVED_BITS are 0 (REQ3.2.1.4)
	* Find the SHA256 of the following data:
		* The transaction's first input "prevout" (hash and index) using standard Bitcoin serialization. *(for entropy)*
		* The scriptPubKey of the first OP_RETURN output (skip if there is no OP_RETURN output) *(commit to the human and wallet-level information and contract)*
		* The Flags field *(this contains a nonce)*
* If the above SHA256 matches the GroupId field<sup>4</sup>, this is a valid genesis UTXO.  Allow this authority UTXO even though the permission it claims are not enabled via an input authority. (REQ3.2.1.5)
* If a subgroup (of this group) output exists, return INVALID *(do not allow subgroup operations in the same transaction as the group genesis)* (REQ3.2.1.6)

This formulation means that a transaction can contain only create one new group.  Also, no other outputs in this transaction can use this group or subgroups of this group (if implementations set all the permissions running flags to 0 this property will be enforced by other rules).  Isolating the genesis UTXO in this manner is an arbitrary limitation intended to improve implementation consistency by reducing edge cases.

It is possible for a subgroup to have no input UTXOs but have outputs because the parent group authority might operate on the subgroup.  An incorrect implementation might misinterpret this as an invalid group genesis output.   As described above, using batonPerms is one way to realize that such an output is not a genesis.  Another (normative) way is that the GroupId for any subgroup must be greater than 32 bytes.

Note that the genesis authority Flags field may not have all permission bits set.  The group creator may want to restrict certain permissions from genesis.

Note that if a covenanted group is created without a RESCRIPT authority, the first MINT transaction can never be valid because there is no input script from which to pull the covenant.  The genesis transaction is valid, but this group is probably useless.

##### 3.1.2.2 Authority Baton Rules

 * For every output authority it MUST be true that  Flags & ~batonPerms == 0.  *(Enforce permissions when passing the baton from input to output -- no bit can be set in Flags, unless it is set in batonPerms)* (REQ3.2.2.1)
 * For every non-genesis output authority it MUST be true that  Flags & ~ALL_FLAG_BITS == 0.  *(Enforce that unused bits are zero, for later use)* (REQ3.2.2.2)
 * For every output (genesis and non-genesis) authority it MUST be true that  Flags & ~RESERVED_FLAG_BITS == 0.  *(Enforce that reserved flag bits are zero, for later use)* (REQ3.2.2.3)

##### 3.1.2.3 Token Quantity Rules

* If the GroupId HOLDS_BCH flag is set, Quantity MUST be 0.  Copy the BCH amount field in every non-authority input and output (grouped) UTXO into the respective Quantity field (REQ3.2.3.1). *(This enables grouped BCH, rather than grouped tokens)*
* The sum of the input Quantities MUST not exceed the maximum int64_t value (REQ3.2.3.2). *(note this is a **signed** 64 bit integer or 0x7FFFF FFFF FFFF FFFF)*
* The sum of the output Quantities MUST not exceed the maximum int64_t value (REQ3.2.3.3). *(note this is a **signed** 64 bit integer or 0x7FFFF FFFF FFFF FFFF)*
* The sum of the input Quantities MUST equal the sum of the output Quantities, unless the group's perms has the MINT or MELT bit set. (REQ3.2.3.4)
* If the group's perms has the MINT bit set, the sum of the output Quantities MUST be >= the sum of the input Quantities. (REQ3.2.3.5)
* If the group's perms has the MELT bit set, the sum of the output Quantities MUST be <= the sum of the output Quantities. (REQ3.2.3.6)

Note that if the group HOLDS_BCH, the MINT and MELT operations are not actually creating or destroying BCH.  Instead these operations act as entry or exit points, allowing BCH to flow into or out of the group.  Conservation of BCH in the transaction is guaranteed because the existing logic that balances BCH in a transaction is untouched.

Note that the total ordinality of the group's tokens may exceed a signed 64 bit integer.  In this case a single transaction cannot manipulate all of them...

##### 3.1.2.4 Covenant Enforcement

Covenants allow spending constraints to be carried from parent to child (to grandchild, etc) transactions.  Any group with the COVENANT bit set enforces covenants by requiring that grouped output scripts match the first grouped prevout script.  To specify an initial script, or offer a script upgrade to holders, an authority with the RESCRIPT bit set can be spent.  This causes the covenant enforcement to be ignored, allowing any output script to be used.

Formally, for every grouped output, if the group has the COVENANT bits set and the group.perm's RESCRIPT bit is clear, all output "scriptpubkey" scripts MUST equal the first grouped non-authority input's prevout "scriptpubkey" script, beginning after the first OP_GROUP instruction (REQ3.2.4.1).  Note that it is equivalent to compare the cryptographic hashes of these script suffixes rather than the script itself.  If this object (the first grouped non-authority) does not exist return INVALID (REQ3.2.4.2).

*(This enforces that covenanted groups use the same constraint script as the first input from that same group<sup>3</sup>.  Efficient implementations should discover this script while iterating through the inputs during a previous step)*

##### 3.1.2.5 Grouped P2SH

The P2SH script type has hard-coded extra-script behavior which is that upon completion the second-to-top stack item (the redeem script) is subsequently executed.  The Grouped P2SH script type (described in chapter 4) MUST behave analogously<sup>5</sup>. (REQ3.2.5.1)

### 4 IsStandard Changes

Two new script types SHOULD be recognized as standard.  These are simply the P2PKH and P2SH script types with the OP_GROUP prefix:

**Grouped P2PKH** (REQ4.1)  
<groupid\> <quantityorflags\> OP_GROUP OP_DUP OP_HASH160 <pubkeyhash\> OP_EQUALVERIFY OP_CHECKSIG

**Grouped P2SH**  (REQ4.2)  
<groupid\> <quantityorflags\> OP_GROUP OP_HASH160 <scripthash\> OP_EQUAL


### Footnotes

1. If, upon review, you find this idea “ugly”, you are welcome within your own software to completely restructure the transaction data as you see fit. When computing the hash of a transaction or serializing transactions or UTXOs for network transmission, simply place the Group data in the serialized format as described.  And start any "grouped" script execution opcode counting as if 3 operations have already been executed.
2. 32 bytes was chosen rather than 20 to prevent a group creator from successfully executing Wagner's birthday attack on their own group identifier.  If a group creator is able to construct a group ID collision, they could break provable assurances (such as total token quantity) by using the colliding genesis transaction to create new authority batons.
3. The covenant is a contract between the group creator and the token holder.  This architecture allows the group creator to *offer* a contract upgrade to token holders.  Token holders accept the new contract by creating a transaction where the 1st grouped input UTXO is the new contract.  But token holders could reject the new contract by continuing to use old-contract UTXOs as the first grouped input.  So the group creator cannot force contract holders to upgrade to a new contract, unless the ability to do so is explicitly coded into the contract by, for example, a clause in the contract that allows a UTXO to be spent if the tx is signed by the group creator.
4. This is beyond the scope of this specification, but for clarity understand that the GroupId's flag bits are part of the hash -- Group creators need to search for a group ID with the desired flag bits in a manner similar to searching for "vanity addresses".
5. A fix to the P2SH "hack" is proposed in the full Group Tokenization document.  Since this fix can happen as a side effect of enabling robust smart contract features, it is better to consider it at that time rather than address it here.

## Implementations

[Showcase implementation](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/src/consensus/grouptokens.cpp)

## Test Cases

[grouptoken_tests.cpp](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/src/test/grouptoken_tests.cpp)
[grouptokens.py](https://gitlab.com/gandrewstone/BCHUnlimited/-/blob/NextChain/qa/rpc-tests/grouptokens.py)

(test vectors) TBD

## Example Uses

[Stablecoin Example](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#stablecoin-example)  
[Atomic Swaps](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#atomic-swaps)  
[CoinJoin](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#coinjoin)  
[NFTs](https://gitlab.com/0353F40E/group-tokenization/-/blob/master/example-uses.md#nfts)

... work in progress.

## Evaluation of Alternatives

We see not having the feature as the only alternative.
Other token proposals are implemented with smart contracts therefore not on the same utility level as native currency and are conceptually different from how the native currency operates.
Our proposal conceptually operates the same way as native currency so downstream developers should not have too much trouble implementing it if they're already familiar with how Bitcoin works.

Other smart contract proposals could operate on grouped tokens, and therefore benefit from grouped tokens, because group tokens provide built-in nouns and a select set of low level verbs.

More generally, we see alternatives in only the rollout approach:

1. We enable native tokens through this proposal,
2. Smart contract solutions either roll their own tokens or use native tokens;

or a simultaneous rollout, or:

1. Roll out a smart contract platform that can roll their own tokens,
2. Wait for the market to demand something more efficient, and then enable native tokens.

The latter approach has the biggest cost in:

- Missed opportunity in the time it takes to build smart contract token systems and evaluate the market response.
- Difficulty in consistently identifying and displaying different smart-contract based tokens in user wallets.
- Technical debt of downstream users having to adapt to new design patterns introduced by the smart contract platform.

We can wait for the market, but the market won't wait for us.
We believe it's reasonable to anticipate that demand if Bitcoin Cash is to get ahead.

We see simultaneous rollout as the preferred option because then synergetic benefit could be achieved right from the start if new smart contracts features would be designed in such way that they can fully use group tokens.

For this to happen, we invite other teams to consider that approach at this early stage in the upgrade cycle, and then a set of features more powerful than any single team could deliver on their own could become available on Bitcoin Cash.

### Industry Acceptance

#### IOTA

The proposed [IOTA tokenization](https://blog.iota.org/tokenization-on-the-tangle-iota-digital-assets-framework/) is a functional subset of Group Tokenization, but notably uses the exact same technique of annotating UTXOs with token identifiers.

#### ION

[ION tokens](https://ionomy.com/) are ported directly from Andrew Stone's work.

#### Blockstream Liquid

["Confidental Assets"](https://blockstream.com/bitcoin17-final41.pdf) uses the same general approach of "...attaching to each output an asset tag identifying the type of that asset, and having verifiers check that the verification equation holds for subsets of the transaction which have only a single asset type."

## Security Considerations

Not even a single line of existing consensus code needs to be changed to implement Group Tokenization.
So implementations should be able to provably show that the addition of Group Tokenization cannot affect the security of the BCH token.

Group Tokenization changes do not add any additional network messages, so do not increase the network attack surface.

Improper implementations of Group Tokenization could cause a blockchain hard fork.
This is true of any miner-validated tokenization system, or of any new feature requiring a hard fork.

## Stakeholder Cost and Benefits Analysis

We present a stakeholder taxonomy.
Everyone should be able to find themselves within at least one group.
We attempt to evaluate the impact of this development on all stakeholders and we invite you to join us, add your names to this list, and comment whether you agree with our assesment of the impact.
We're happy to address any concerns or questions you may have.

General costs and benefits:

- (+) Competitive advantage in comparison to other blockchains/ecosystems.
- (+) This functionality will enable competition from within our ecosystem.
Some may feel threatened by this, but what is the alternative?
To have the competition build outside of our ecosystem and let them create value elsewhere?
- (+) Attracting new users, talent, and capital from outside thus helping grow the entire ecosystem centered around Bitcoin Cash.
Conceptually simple tools mean more potential builders.
- (+) Opportunity cost.
Success of tokens is a good measure of that.
Missed "revenue" in terms of user, talent, and capital inflows.
Missed ecosystem growth opportunity.
- (+) Future proof.
Since every group token is also a BCH output, it should seamlessly work with everything that can work with BCH, including Script and any future smart contract platform.
- (+) Every grouped output created also creates marginal demand for BCH.
- (+) Increased Bitcoin Cash adoption and utility, as the "first among equals" currency that powers the Bitcoin Cash blockchain.
- (+) Low risk to native currency security, as consensus rules are only added in
a self-contained way and toggled through a single opcode.
Existing rules are not changed.
- (-) Potential to speed up adoption.
While this benefits BCH, it could create problems of its own.
We don't see adoption creating problems as a realistic outcome, but this perspective is worth consideration, and it would be a good problem to have.

### 1 Node Stakeholders

- (-) Marginally more resource usage when compared to the simplest BCH UTXO.

#### 1.1 Miners

- (+) More fee revenue through increased demand for transactions.

#### 1.2 Big nodes

Exchanges, block explorers, backbone nodes, etc.

- (+) Easy to implement other currencies through same familiar design patterns that are now used for Bitcoin Cash alone.

#### 1.3 User nodes

Hobbyiest full nodes.

#### 1.4 Node developers

- (-) One-off workload increase
- (+) Later it just works out of the box, downstream developers create utility.

### 2 Userspace stakeholders

- (+) Barrier to entry to token usage lowered.

#### 2.1 Business users

- (+) Access to financial instruments previously not available, or access to them at a better price and quality than competition can provide.
- (+) Enables potential new business models.

#### 2.2 Userspace developers

- (+) Easy to implement other currencies through the same familiar design patterns that are now used for Bitcoin Cash alone.
- (+) With more growth there will be more opportunity for professional advancement within the ecosystem.

#### 2.3 Individual users

- (+) Get to enjoy the best peer-to-peer electronic cash system!

### 3 Holders and speculators

#### 3.1 Long

- (+) More adoption correlates with increase in price.
- (+) Even other tokens marginally increase demand for BCH, so the more
transactions of any kind, the better.

#### 3.2 Short

- (+) More liquidity in the market. Following Bitcoin Cash may help you decide to switch side and benefit from it!

### 4 Opinion holders who are not stakeholders

We are hoping developments like this will move you towards becoming stakeholders!

## Statements

## Future Work

[Full future functional specification and upgrade path.](https://docs.google.com/document/d/1X-yrqBJNj6oGPku49krZqTMGNNEWnUJBRFjX7fJXvTs/edit)

## Discussions

[CHIP Discussion](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311)
[Previous Discussion](https://bitcoincashresearch.org/t/native-group-tokenization/278)

## Copyright

MIT Licensed
